<?php
namespace vendor\pillax\router\src;
/**
 * Class router
 * @package vendor\pillax\router
 *
 * To work properly, add this to your .htaccess file:
 *
 * RewriteEngine On
 * RewriteCond %{REQUEST_FILENAME} !-d
 * RewriteRule ^(.*)/$ /$1 [L,R] # <- for test, for prod use [L,R=301]
 * RewriteCond %{REQUEST_FILENAME} !-f
 * RewriteCond %{REQUEST_FILENAME} !-d
 * RewriteCond %{REQUEST_FILENAME} !-l
 * RewriteRule ^(.+)$ index.php?url=/$1 [QSA,L]
 *
 */
class Router {
    /** @var router  */
    private static $instance;

    /** @var array  */
    private $routes = [];

    /** @var array  */
    private $allowedRequestMethods = ['POST', 'GET'];

    /** @var array  */
    private $regexShortcuts = [
        'i'  => '([0-9]{1,})',
        'a'  => '([0-9A-Za-z]{1,})',
        'c'  => '([a-zA-Z0-9+_\-\.]{1,})'
    ];

    /**
     * Singleton
     *
     * @return router
     */
    public static function init() : router {
        if( ! self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Simple regex to find url pattern replacements
     *
     * @return string
     */
    private function getRegexShortcutsPattern() : string {
        return '@\{([a-zA-Z0-9_\-]{1,})\:([' . implode('|', array_keys($this->regexShortcuts)) . '])\}@';
    }

    /**
     * Adding route
     *
     * Example: using post and get
     *  router::init()->addRoute('GET', '/test', 'app\controllers\testController#testme');
     *  router::init()->addRoute('POST', '/test', 'app\controllers\testController#testme');
     *  router::init()->addRoute('GET|POST', '/test', 'app\controllers\testController#testme');
     *
     * Example: using closure
     *  router::init()->addRoute('GET', '/test/{id:i}', function($id) {
     *      echo 'Id is ' . $id;
     *  });
     *
     * Example: regex shortcuts
     *  router::init()->addRoute('GET', '/{controller:a}/{action:a}/{id:i}', 'app\controllers\testController#testme');
     *  action testme should looks like this:
     *  public function testMe($controller, $action, $id) {
     *      // Parameters are in THE SAME ORDER as in the route!
     *  }
     *
     * Example: how to order your routes - the longest one is on the top
     *  router::init()->addRoute('GET', '/news/{id:i}', 'app\controllers\testController#news');
     *  router::init()->addRoute('GET', '/news', 'app\controllers\testController#allNews');
     *  router::init()->addRoute('GET', '/', 'app\controllers\testController#index');
     *
     *
     * @param string $method request method GET|POST
     * @param string $pattern Url pattern associated with this route
     * @param string|\Closure $action function that will be called if route is matched.
     * @param null|string $name used in $this->getRoute(...)
     */
    public function addRoute(string $method, string $pattern, $action, ?string $name=null) : void {
        $this->routes[] = [
            'method'    => explode('|', $method),
            'pattern'   => $pattern,
            'action'    => $action,
            'name'      => $name,
        ];
    }

    /**
     * Trying to match each $this->routes to $url and when succeed calling route action
     *
     * @param string $url
     * @param string $requestMethod POST|GET
     * @return bool
     * @throws routerException
     */
    public function match(string $url, string $requestMethod) : bool {
        foreach ($this->routes AS $route) {

            // Skip iteration, when request method doesn't matching the user defined request method
            if( ! in_array($requestMethod, $route['method'])) {
                continue;
            }

            // default route pattern
            $pattern = $route['pattern'];

            // Check for special symbols in current pattern
            if(preg_match_all($this->getRegexShortcutsPattern(), $route['pattern'], $matches)) {
                $targets = [];
                $replacements = [];
                foreach($matches[0] AS $key => $patternElement) {
                    $targets[]      = $patternElement;
                    $replacements[] = $this->regexShortcuts[$matches[2][$key]];
                }
                $pattern = str_replace($targets, $replacements, $pattern);
            }

            // Matching route
            preg_match('@^' . $pattern . '$@', $url, $matches);

            if($matches) {
                // remove full path - we ain't going to use it
                unset($matches[0]);

                if($route['action'] instanceof \Closure) {
                    // on Closure callback
                    call_user_func_array($route['action'], $matches);
                } else {
                    // On path to namespace\className
                    list($controller, $action) = explode('#', $route['action']);

                    if( ! is_callable([$controller, $action])) {
                        throw new routerException(routerException::NOT_FOUND, [
                            'url'    => $url,
                            'method' => $requestMethod,
                            'error'  => $route['action'] . ' is not callble',
                        ]);
                    }

                    $instance = new $controller;
                    call_user_func_array([$instance, $action], $matches);
                }

                // Terminate execution
                return true;
            }
        }

        // When no matches found - show 404 page
        throw new routerException(routerException::NOT_FOUND, [
            'url'    => $url,
            'method' => $requestMethod,
        ]);
    }

    /**
     * Finds route by $name and build path according route pattern
     *
     * Example:
     * We have this route defined:
     * router::init()->addRoute('GET', '/{controller:a}/{action:a}/{id:i}', 'app\controllers\testController#testme', 'fooBar');
     *
     * We should call it in this way:
     *  $link = router::init()->getRoute('fooBar', [
     *      'controller' => 'news',
     *      'action' => 'read',
     *      'id' => 22,
     *  ]);
     *
     * Result:
     * $link is /news/read/22
     *
     * @param string $name
     * @param array $params assoc array with substitutionName => substitutionValue
     * @return string
     */
    public function getRoute(string $name, array $params=[]) : string {
        // Find route by name
        $key = array_search($name, array_column($this->routes, 'name'));
        $route = $this->routes[$key];

        // Default
        $pattern = $route['pattern'];

        // On special symbols
        if(preg_match_all($this->getRegexShortcutsPattern(), $route['pattern'], $matches)) {
            $targets = [];
            $replacements = [];
            foreach($matches[0] AS $key => $patternElement) {
                $targets[]      = $patternElement;
                $replacements[] = $params[$matches[1][$key]];
            }
            $pattern = str_replace($targets, $replacements, $pattern);
        }

        return $pattern;
    }

    /**
     * Prevent class normal initializing
     */
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

}